# !/bin/bash
# List of directories you want this script to backup.
DIRS=(
	'/home/user'
	'/var'
	'/etc'
	'/var/www')

# Directory where you want all of the backups to be saved to.
BACKUP_PLACE='/BACKUP'




# Performs backup job. Best to leave the code below untouched unless you're a scary Linux user.
echo ' ==========[BACKUP]=========='
echo ' Starting backup...'
if [ ! -d "$BACKUP_PLACE" ]; then
	mkdir $BACKUP_PLACE
fi
cd $BACKUP_PLACE
DATE_FILE=$((date +%a_%b_%d_%Y-%T))
mkdir $DATE_FILE
cd $DATE_FILE
CNT=0
for DIR in '${DIRS[@]}';
do
	COUNT=$(($CNT + 1))
	echo " Backing up '$DIR'..."
	tar -czvf $DIR.tar.gz $DIR
	echo " Finished backing up '$DIR'."
done
echo ' Backup complete!'
echo ' ==========[BACKUP]=========='
