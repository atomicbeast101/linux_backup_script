# Linux Backup script
Custom backup script for Linux server(s).

## Requirements
- Linux OS

## Setup

1) Download the files and export them to any directory on Linux OS.

2) Set the permissions for Backup.sh file:
```Bash
sudo chmod 777 /path/to/Backup.sh
```

4) Change the list of directories/files you want this script to backup:
```Bash
DIRS=(
	'/home/user'
	'/var'
	'/etc'
	'/var/www')
```

5) Change the location of the backups directory:
```Bash
BACKUP_PLACE='/BACKUP'
```

6) Perform backup:
```Bash
./path/to/Backup.sh
```

7) (Optional) Setup auto backup:
```Bash
crontab -e
```
Setup how often you want to backup:
```
minute hour day month day-of-week /path/to/Backup.sh
```
Tip:
```
* * * * * /path/to/Backup.sh
- - - - -
| | | | |
| | | | ----- Day of week (0 - 7)**
| | | ------- Month (1 - 12)
| | --------- Day (1 - 31)
| ----------- Hour (0 - 23)
------------- Minute (0 - 59)

**
0 OR 7 = Sunday
1 = Monday
2 = Tuesday
3 = Wednesday
4 = Thursday
5 = Friday
6 = Saturday

* = All
```
More guides here: https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/

Example #1 - Backup every day at midnight:
```
0 0 * * * /path/to/Backup.sh
```
Example #2 - Backup every Wednesday:
```
* * * * 3 /path/to/Backup.sh
```
Example #3 - Backup at 10pm every Sunday night:
```
0 22 * * 7 /path/to/Backup.sh
```
